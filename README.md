#Project setup details
1)Install node version v8.15.1.
2)Install mongodb version v2.6.10
3)Check NPM module versions in package.json in project root directory.
4)Clone from the git url | https://gitlab.com/sidhilsivadas/nodeapi.git
5)Start server typing node server.js (default port is 3001)
6)Check log.txt you will see 'Server is listening on port 3001' if everything is properly installed.
NOTE : Every console.log messages are write in to log.txt file.
