const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
    name : String,
    slug : String,
    parent_category : String,
    parent_tree : []
}, {
    timestamps: true
});
module.exports = mongoose.model('Category', CategorySchema);
