const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
    name : String,
    slug : String,
    price : String,
    category : String,
    }, {
    timestamps: true
});
module.exports = mongoose.model('Products', CategorySchema);
