const categoryController = require('../controllers/category.controller.js');
const productController = require('../controllers/product.controller.js');

class Routes{
  constructor(app){
		this.app = app;
	}
  serverRoutes(){
    this.app.post('/category', categoryController.create);
    //GetCategories
    this.app.get('/category', categoryController.fetchCategories);
    //this.app.get('/category/:id', categoryController.getSingleCategory);
    //this.app.put('/category/:id', categoryController.updateCategory);

    // products apis
     this.app.post('/product', productController.create);
     this.app.get('/category/products/:category_id', productController.fetchProductsByCategory);
    // this.app.get('/products/:id', productController.getSingleProduct);
     this.app.put('/product/:id', productController.updateProduct);
  }
  initRoutes(){
		this.serverRoutes();
	}
}

module.exports = Routes;
