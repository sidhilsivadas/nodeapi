const CategoryRepository = require('../repository/category.repository');
const ProductRepository = require('../repository/product.repository');
const common = require('../../src/config/common');


class ProductController{



  async create(req, res){
    //console.log("dd"+JSON.stringify(req.body));
   if(common.isEmptyObject(req.body)){
     common.set_output(res,{'status':false,'message':'Request body cannot be empty'},400);
     return;
   }

   if(common.isEmptyObjectKey(req.body,"name")){
     common.set_output(res,{'status':false,'message':'Name cannot be empty'},400);
     return;
   }
   var category_ids = [];
   if(!common.isEmptyObjectKey(req.body,"category_ids")){
       category_ids = req.body.category_ids;
     if( typeof category_ids === 'string' ) {
       category_ids = [ category_ids ];
     }
   }else{
     common.set_output(res,{'status':false,'message':'category_ids cannot be empty'},400);
     return;
   }

   req.body.category_ids = category_ids;
   var name = req.body.name;
   var slug = common.makeSlugByName(name);
   req.body.slug = slug;
   let cat = await ProductRepository.getSingleProductBySlug(req, res,slug);
   if(!common.isEmptyObject(cat)){
     common.set_output(res,{'status':false,'message':'Product Exist'},422);
     return;
   }

   let insert_data = await ProductRepository.createSingleProduct(req, res);
   //console.log(insert_data.insertedId);
   if(!common.isEmptyObjectKey(insert_data,'insertedId')){

     common.set_output(res,{"status":true,"message":"Product created","data":{'id':insert_data.insertedId,'name':req.body.name,'slug':slug,'price':req.body.price || null,'category_ids':category_ids}},200);
     return;
   }else{
     common.set_output(res,{"status":false,"message":"error in Product creation!Inform developer","data":insert_data},500);
     return;
   }

 }

 async fetchProductsByCategory(req, res){
   if(common.isEmptyObjectKey(req.params,"category_id")){
     common.set_output(res,{'status':false,'message':'Please provide a category_id'},401);
     return;
   }
   var prod = await ProductRepository.fetchProductsByCategory(req, res,req.params.category_id);
   common.set_output(res,{"status":true,"data":prod},200);
   return;
 }

 async updateProduct(req, res){

   if(common.isEmptyObjectKey(req.params,"id")){
     common.set_output(res,{'status':false,'message':'Please provide a product_id'},400);
     return;
   }

   var product_id = req.params.id;

   var prod = await ProductRepository.getSingleProductById(req, res,product_id);
   if(common.isEmptyObject(prod)){
     common.set_output(res,{'status':false,'message':'Product not found'},401);
     return;
   }


   if(common.isEmptyObject(req.body)){
     common.set_output(res,{'status':false,'message':'Request body cannot be empty'},400);
     return;
   }

   if(common.isEmptyObjectKey(req.body,"name")){
     common.set_output(res,{'status':false,'message':'Name cannot be empty'},400);
     return;
   }
   var category_ids = [];
   if(!common.isEmptyObjectKey(req.body,"category_ids")){
       category_ids = req.body.category_ids;
     if( typeof category_ids === 'string' ) {
       category_ids = [ category_ids ];
     }
   }else{
     common.set_output(res,{'status':false,'message':'category_ids cannot be empty'},400);
     return;
   }



   req.body.category_ids = category_ids;
   var name = req.body.name;
   var slug = common.makeSlugByName(name);
   req.body.slug = slug;
   req.body.id = product_id;
   let slug_prod = await ProductRepository.getSingleProductBySlug(req, res,slug);
   if(!common.isEmptyObject(slug_prod) && slug_prod._id != product_id){
     common.set_output(res,{'status':false,'message':'Product Exist'},422);
     return;
   }

   var update_arr = {'name':req.body.name,'slug':slug,'category_ids':category_ids};
   if(!common.isEmptyObjectKey(req.body,"price")){
     update_arr.price = req.body.price;
   }
   let update_data = await ProductRepository.updateProduct(req, res,update_arr);
   //console.log(insert_data.insertedId);
   if(!common.isEmptyObject(update_data)){
     common.set_output(res,{"status":true,"message":"Update Success","data":update_data},200);
     return;
   }else{
     common.set_output(res,{"status":false,"message":"error in Product updation!Inform developer","data":update_data},500);
     return;
   }

 }

}

module.exports = new ProductController();

// Create and Save a new Note
// Create and Save a new Note
