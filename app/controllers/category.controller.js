const CategoryRepository = require('../repository/category.repository');
const common = require('../../src/config/common');


class CategoryController{



  async create(req, res){
    //console.log("dd"+JSON.stringify(req.body));
   if(common.isEmptyObject(req.body)){
     common.set_output(res,{'status':false,'message':'Request body cannot be empty'},400);
     return;
   }

   if(common.isEmptyObjectKey(req.body,"name")){
     common.set_output(res,{'status':false,'message':'Name cannot be empty'},400);
     return;
   }
   var name = req.body.name;
   var slug = common.makeSlugByName(name);
   req.body.slug = slug;
   let cat = await CategoryRepository.getSingleCategoryBySlug(req, res,slug);
   if(!common.isEmptyObject(cat)){
     common.set_output(res,{'status':false,'message':'Category Exist'},422);
     return;
   }
   //console.log("sds");
   //checking parent catehory
   if(common.isEmptyObjectKey(req.body,"parent_category")){
     req.body.parent_category = [];
     var parent_tree = [];
   }else{
     //console.log(req.body.parent_category);
     var parent_category = req.body.parent_category;
     //req.params.id = parent_category;
     var parent_cat = await CategoryRepository.getSingleCategoryById(req, res,parent_category);
     //console.log(parent_cat);
     if(common.isEmptyObject(parent_cat)){
       common.set_output(res,{'status':false,'message':'Parent Category Is Not Exist'},401);
       return;
     }
     //var parent_category_slug = common.makeSlugByName(parent_category);
     req.body.parent_category = parent_cat;
     var parent_tree = parent_cat.parent_tree;
      if(!common.isEmptyObject(parent_tree)){
        parent_tree.push({'_id':parent_cat._id,'name':parent_cat.name,'slug':parent_cat.slug});
      }else{
        parent_tree = [{'_id':parent_cat._id,'name':parent_cat.name,'slug':parent_cat.slug}];
      }


   }
   req.body.parent_tree = parent_tree;
   //console.log("ssss"+parent_tree);
   //
   let insert_data = await CategoryRepository.createSingleCategory(req, res);
   //console.log(insert_data.insertedId);
   if(!common.isEmptyObjectKey(insert_data,'insertedId')){

     common.set_output(res,{"status":true,"message":"Category created","data":{'id':insert_data.insertedId,'name':req.body.name,'slug':slug,'parent_category':parent_cat,'parent_tree':req.body.parent_tree}},200);
     return;
   }else{
     common.set_output(res,{"status":false,"message":"error in Category creation!Inform developer","data":insert_data},500);
     return;
   }

 }

 async fetchCategories(req, res){
   var out_arr = [];
   let cats = await CategoryRepository.fetchCategories(req, res);
   if(!common.isEmptyObject(cats)){
     for (const val of cats) {
       var id = val._id;
       var obj = {};
       obj.id = id;
       obj.name = val.name;
       obj.slug = val.slug;
       obj.child_categories = [];
       var child_cats = await CategoryRepository.fetchCategoryByParentId(req,id);
       if(!common.isEmptyObject(child_cats)){
         for (const val_child of child_cats) {
           var child_cat_obj = {};
           child_cat_obj.id = val_child._id;
           child_cat_obj.name = val_child.name;
           child_cat_obj.slug = val_child.slug;
           obj.child_categories.push(child_cat_obj);
         }

       }
       out_arr.push(obj);


      console.log(val._id);
     }
   }
   common.set_output(res,{"status":true,"data":out_arr},200);
   return;
 }

 async getSingleCategory(req, res){
   if(common.isEmptyObjectKey(req.params,"id")){
     common.set_output(res,{'status':false,'message':'Please provide a Category id'},401);
     return;
   }
   var cat = await CategoryRepository.getSingleCategoryById(req, res,req.params.id);
   common.set_output(res,{"status":true,"data":cat},200);
   return;
 }

 async updateCategory(req, res){
   if(common.isEmptyObjectKey(req.params,"id")){
     common.set_output(res,{'status':false,'message':'Please provide a Category id'},401);
     return;
   }
   if(common.isEmptyObject(req.body)){
     common.set_output(res,{'status':false,'message':'Request body cannot be empty'},400);
     return;
   }
   let catbyslug = await CategoryRepository.getSingleCategoryById(req, res,req.params.id);
   if(common.isEmptyObject(catbyslug)){
     common.set_output(res,{'status':false,'message':'Category Is Not Exist'},401);
     return;
   }

   if(common.isEmptyObjectKey(req.body,"name")){
     common.set_output(res,{'status':false,'message':'Name cannot be empty'},400);
     return;
   }
   var name = req.body.name;
   var slug = common.makeSlugByName(name);
   req.body.slug = slug;
   let cat = await CategoryRepository.getSingleCategoryBySlug(req, res,slug);
   if(!common.isEmptyObject(cat) && cat['_id'] != req.params.id){
     common.set_output(res,{'status':false,'message':'Category Exist'},422);
     return;
   }


   //checking parent catehory
   if(common.isEmptyObjectKey(req.body,"parent_category")){
     req.body.parent_category = [];
     var parent_tree = [];
   }else{
     //console.log(req.body.parent_category);
     var parent_category = req.body.parent_category;
     //console.log(parent_category);
     //req.params.id = parent_category;
     var parent_cat = await CategoryRepository.getSingleCategoryById(req, res,parent_category);
     //console.log(parent_cat);
     if(common.isEmptyObject(parent_cat)){
       common.set_output(res,{'status':false,'message':'Parent Category Is Not Exist'},401);
       return;
     }
     //var parent_category_slug = common.makeSlugByName(parent_category);
     req.body.parent_category = parent_cat;
     if(!common.isEmptyObject(cat) && (parent_category === cat.parent_category._id)){
       var parent_tree = parent_cat.parent_tree;
     }else{
        var parent_tree = parent_cat.parent_tree;
        if(!common.isEmptyObject(parent_tree)){
          parent_tree.push({'_id':parent_cat._id,'name':parent_cat.name,'slug':parent_cat.slug});
        }else{
          parent_tree = [{'_id':parent_cat._id,'name':parent_cat.name,'slug':parent_cat.slug}];
        }
     }



   }
   req.body.parent_tree = parent_tree;

   let update_arr = { 'name': req.body.name,'slug':req.body.slug,'parent_category':req.body.parent_category,'parent_tree':req.body.parent_tree };
   //console.log(update_arr);
   var update_res = await CategoryRepository.upadteCategory(req, res,update_arr);
   //console.log(update_res);
   common.set_output(res,{"status":true,"data":update_res},200);
   return;
 }

}

module.exports = new CategoryController();

// Create and Save a new Note
// Create and Save a new Note
