var ObjectID = require('mongodb').ObjectID;
class ProductRepository{

	constructor(){



  }


  



async getSingleProductBySlug(req,res,slug) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('products');
        //db.collection( 'users' ).find();

        let query = { slug: slug }

        let res = await collection.findOne(query);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return [];
    }
}

async getSingleProductById(req,res,id) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('products');
        //db.collection( 'users' ).find();

        let query = { _id: new ObjectID(id) }

        let res = await collection.findOne(query);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return [];
    }
}

async createSingleProduct(req,res) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('products');
        //db.collection( 'users' ).find();

        let insert_arr = { 'name': req.body.name,'slug':req.body.slug,'price':req.body.price || null,'category_ids':req.body.category_ids };
        //console.log(insert_arr);
        let res = await collection.insertOne(insert_arr);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return null;
    }
}


async fetchProductsByCategory(req,res,category_id){
	const db = req.db;

  try {
      let collection = db.collection('products');
			let query = { category_ids: category_id }
      let res = await collection.find(query).toArray();
      return res;

  } catch (err) {

      console.log(err);
      return null;
  }
}

async updateProduct(req,res,update_arr) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('products');
        let query = { _id: new ObjectID(req.params.id) }
        //console.log(query);
        let res = await collection.update(query,{$set:update_arr});

        return res;

    } catch (err) {

        console.log(err);
        return null;
    }
}




}

module.exports = new ProductRepository();
