var ObjectID = require('mongodb').ObjectID;
class CategoryRepository{

	constructor(){



  }


  async setMongoConnection(){
    this.db  = await DB.connect();
  }



async getSingleCategoryBySlug(req,res,slug) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('categories');
        //db.collection( 'users' ).find();

        let query = { slug: slug }

        let res = await collection.findOne(query);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return [];
    }
}

async getSingleCategoryById(req,res,id) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('categories');
        //db.collection( 'users' ).find();

        let query = { _id: new ObjectID(id) }

        let res = await collection.findOne(query);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return [];
    }
}

async createSingleCategory(req,res) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('categories');
        //db.collection( 'users' ).find();

        let insert_arr = { 'name': req.body.name,'slug':req.body.slug,'parent_category':req.body.parent_category,'parent_tree':req.body.parent_tree };
        //console.log(insert_arr);
        let res = await collection.insertOne(insert_arr);
        //console.log(res);
        return res;

    } catch (err) {

        console.log(err);
        return null;
    }
}

async fetchCategories(req,res){
  const db = req.db;

  try {
      let collection = db.collection('categories');
      let res = await collection.find().toArray();
      return res;

  } catch (err) {

      console.log(err);
      return null;
  }
}

async upadteCategory(req,res,update_arr) {
    const db = req.db;

    try {

        //const db = client.db("node-mongo-api");

        let collection = db.collection('categories');
        let query = { _id: new ObjectID(req.params.id) }
        //console.log(query);
        let res = await collection.update(query,{$set:update_arr});

        return res;

    } catch (err) {

        console.log(err);
        return null;
    }
}

async fetchCategoryByParentId(req,id){
  const db = req.db;

  try {
      let collection = db.collection('categories');
			let query = { "parent_category._id": new ObjectID(id) };
      let res = await collection.find(query).toArray();
      return res;

  } catch (err) {

      console.log(err);
      return null;
  }
}


}

module.exports = new CategoryRepository();
