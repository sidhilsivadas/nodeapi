const MongoClient = require('mongodb').MongoClient
const mongo_url = "mongodb://localhost:27017";

var client;

async function connectDB() {
    if (!client) client = await MongoClient.connect(mongo_url, { useNewUrlParser: true });

    return {
        db: client.db('node-mongo-api'),
        client: client
    };
}
