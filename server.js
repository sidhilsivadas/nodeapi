const express = require('express');
const bodyParser = require('body-parser');
const loger = require('./src/config/loger');
const routes = require('./app/routes/routes');
console.file("./log.txt"); //Writing console.log output to file for developer easy debug.
var expressMongoDb = require('express-mongo-db');
class Server{

	constructor(){
		this.port = 3001;
		this.app = express();
	}

	setupMiddleware(){
    this.app.use(expressMongoDb('mongodb://localhost/node-mongo-api'));
    // parse requests of content-type - application/x-www-form-urlencoded
    this.app.use(bodyParser.urlencoded({ extended: true }))
    // parse requests of content-type - application/json
    this.app.use(bodyParser.json());
	}

	setupRoutes(){
		new routes(this.app).initRoutes();
		//new socketHanlder(this.socket).socketConfig();
	}

	start(){
		this.setupMiddleware();
		this.setupRoutes();
    this.app.listen(this.port, () => {
        console.log("Server is listening on port "+this.port);
    });
	}
}

const server = new Server();
server.start();
/*
	To Run Locally: node lib/server.js 3000 conf/application_config.json
*/
